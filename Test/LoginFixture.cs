﻿using System;

namespace Test
{
    /// <summary>
    /// Log on to Milestones system.
    /// The management service is the service that validates the credentials.
    /// In the below it is assumed to be running on your local machine.
    ///
    /// Please observe the "VideoOS.Platform.SDK.Environment.Initialize()". Without this
    /// none of the environment calls will be running. 
    /// </summary>
    public class LoginFixture : IDisposable
    {
        public LoginFixture()
        {
            VideoOS.Platform.SDK.Environment.Initialize();
            if (!Login("Negotiate")) // either Basic or Negotiate
            {
                throw new Exception("Login failed");
            }
        }

        public bool Login(string negotiateOrBasic)
        {
            var uri = new Uri("http://localhost");
            dynamic credentialCache;
            switch (negotiateOrBasic)
            {
                case "Basic":
                    credentialCache = VideoOS.Platform.Login.Util.BuildCredentialCache(uri, "Jakob", "Jakob", "Basic");
                    break;
                case "Negotiate":
                    credentialCache = VideoOS.Platform.Login.Util.BuildNetworkCredential(uri, "", "", "Negotiate");
                    break;
                default: throw new ArgumentException($"Invalid argument: {negotiateOrBasic}");
            }

            VideoOS.Platform.SDK.Environment.RemoveAllServers();
            VideoOS.Platform.SDK.Environment.AddServer(uri, credentialCache);

            try
            {
                VideoOS.Platform.SDK.Environment.Login(uri);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public void Dispose()
        {
        }
    }
}
