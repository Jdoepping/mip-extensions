﻿using System;
using System.Threading.Tasks;
using Extensions.Messaging;
using Extensions.SmartClientNotification;
using Extensions.SystemObjects;
using VideoOS.Platform;
using VideoOS.Platform.Messaging;
using Xunit;

namespace Test.SmartClient.Notifications
{
    public class SmartClientNotificationTests
    {
        public const string BroadcastMessageId = "www.companyname.appname.broadcastMessage";
        /// <summary>
        /// Run this test while observing the SmartClient or debug through it.
        /// Be aware that the SmartClient must have loaded the "BroadCastListener" plug-in.
        /// The reason for this is stated in the first "NB NB..." section.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task RaiseSmartClientNotificationTest()
        {
            var login = new LoginFixture();

            #region Arrange

            var messenger = new SimpleMessaging(MessagingScope.External);

            var notification = new SmartClientMessageData
            {
                ButtonText = "ButtonText", // When the text is not null a button is displayed. Setup a listener for  
                // MessageId.SmartClient.SmartClientMessageButtonClickedIndication to handle button clicks     
                IsClosable = false, // Makes it possible for the operator to close the message 
                Message = "Message", // An empty message removes a message 
                MessageId = 1, // A message id. A running task with a progress bar must update on the same id.  
                MessageType = SmartClientMessageDataType.Task, // Shows a task bar when set to Task
                Priority = SmartClientMessageDataPriority.Normal, // Not used, has no meaning 
                TaskProgress = 0, // values between 0 and 1. The message will be changed if any values are changed e.g. if IsClosable is changed from true to false the message cannot be close by the operator anymore.
                TaskState = SmartClientMessageDataTaskState.Complete, // State of a task
                TaskText = "Task Text" // A message in the task area.
            };
            // NB NB NB NB NB NB NB NB NB NB NB NB NB NB NB NB NB
            /* The messageId for notifications i.e. "SmartClientMessageCommand" ends in "Command". Under the normal
             security settings, all messageIds ending in command send from outside the SmartClient are ignored. 
             So, in order to make this test work. I created a plug-in that instead listens for the "BroadcastMessageId".
             Upon receiving such a message it will send the event inside the SmartClient, with the messageId
             found in the "ObjectIdString" property of the "destinationFqid" parameter for the transmit method.
             Oh dear, what a mess.
             */
            var destinationFqid = new FQID
            {
                ObjectIdString = MessageId.SmartClient.SmartClientMessageCommand,
            };
            var envelopedMessage = new Message(BroadcastMessageId, notification);
            void Send() => messenger.ExternalMessenger.TransmitMessage(envelopedMessage, null, destinationFqid, null); 

            #endregion Arrange

            // NB NB NB NB NB NB NB NB NB NB NB NB
            // Please be aware that the properties of "notification" is changed and then send to the smart client. 
            // Also be aware that extension methods are used that are not part of the MIP SDK. 

            // Observe the effect in the smart client.
            // The initial notification is send. Should be visible in the Smart Client.
            Send();

            await Task.Delay(2.Seconds());
            await UpdateProgressbar(notification, Send);
            await Task.Delay(2.Seconds());

            notification.RemoveMessage();
            Send();

            // Clean-up
            messenger.Close();
        }

        private async Task UpdateProgressbar(SmartClientMessageData notification, Action send)
        {
            for (var i = 1; i <= 10; i++)
            {
                await Task.Delay(1.Seconds());

                notification.UpdateProgressbar(0.1 * i);
                send();
            }
        }
    }
}
