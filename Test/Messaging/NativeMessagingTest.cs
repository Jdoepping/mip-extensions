﻿using System;
using System.Linq;
using System.Threading;
using Extensions.SystemObjects;
using VideoOS.Platform;
using VideoOS.Platform.Messaging;
using Xunit;

namespace Test.Messaging
{
    /// <summary>
    /// Native messaging (not using my amazing wrapper for the messaging providers)
    ///
    /// Remember that there is two messaging providers.
    /// 1. EnvironmentManager.Instance
    /// 2. MessageCommunication
    /// 
    /// </summary>
    public partial class MessagingTest: IClassFixture<LoginFixture>, IDisposable
    {
        private readonly LoginFixture login;
        private string mipMessagingReceiverMarker = "Nothing Received waiting...";
        public MessagingTest(LoginFixture login) => this.login = login;

        #region Messaging using the EnvironmentManager
        /// <summary>
        /// The scope of this messaging is the current application (the unit test environment). It is 
        /// therefore possible to use the messaging mechanism provided by the "EnvironmentManager".
        /// Please observe that the "EnvironmentManager.Instance" is initialized in the login fixture.
        /// Go check the "LoginFixture.cs" file and notice the line:
        /// VideoOS.Platform.SDK.Environment.Initialize()
        ///
        /// The below test does it all.
        ///     1. Define a message identity
        ///     2. Register a message handler and start listening for the message id.
        ///     3. Send message.
        ///     4. Un-register to clean up.
        /// </summary>
        [Fact]
        [Trait("Category", "Native Messaging")]
        public void SendMethodTest()
        {
            // Define a message id. Make it unlikely to collide with other message ids.
            var messageId = "com.acme.MessageIdSample";
            // Register a message handler and start listening
            var obj = EnvironmentManager.Instance.RegisterReceiver(ReceiveMyMessagePlease, new MessageIdFilter(messageId));
            // Send a message and wait for all listeners to reply. 
            var returnValues = EnvironmentManager.Instance.SendMessage(new Message(messageId));
            Assert.True((string)returnValues.First() == "MessageReceived");
            // Un-register. Don't introduce memory leaks...
            EnvironmentManager.Instance.UnRegisterReceiver(obj);
        }

        /// <summary>
        /// Understanding that an EnvironmentManager.Instance.SendMessage call, returns a reply from
        /// all listeners.
        /// </summary>
        [Fact]
        [Trait("Category", "Native Messaging")]
        public void MultipleListenersTest()
        {
            // Define a message id
            var messageId = "com.acme.MessageIdSample01";
            // Register a message handler and start listening
            var obj01 = EnvironmentManager.Instance.RegisterReceiver(ReceiveMyMessagePlease, new MessageIdFilter(messageId));
            var obj02 = EnvironmentManager.Instance.RegisterReceiver(ReceiveMyMessagePlease, new MessageIdFilter(messageId));
            // Send a message and wait for all listeners to reply. 
            var returnValues = EnvironmentManager.Instance.SendMessage(new Message(messageId));
            var replyCount = 2;
            Assert.Equal(replyCount, returnValues.Count());
            // Un-register. Don't introduce memory leaks...
            EnvironmentManager.Instance.UnRegisterReceiver(obj01);
            EnvironmentManager.Instance.UnRegisterReceiver(obj02);
        }

        /// <summary>
        /// In Milestones documentation you can find build in messages identities.
        /// Be aware that these in a sense are environment specific.
        /// In this test a build in message id is used. It is, however, a message id that the
        /// SmartClient is listening for and none of the other environments.
        /// Especially in this environment (unit test) there is no listener setup for the message id
        /// and hence no message handler either.
        ///
        /// Observe:
        ///     1. A build in message id is used
        ///     2. Consider the scope i.e. where is your code running and is there a listener setup in that environment.
        ///     3. You can setup your own listener. 
        /// </summary>
        [Fact]
        [Trait("Category", "Native Messaging")]
        public void BuildInMessageIdExampleTest()
        {
            // Use a build in message id. 
            var messageId = VideoOS.Platform.Messaging.MessageId.SmartClient.GetCurrentWorkspaceRequest;
            var replies = EnvironmentManager.Instance.SendMessage(new Message(messageId));
            // No one replies, because in the current environment, nobody is listening for that message.
            Assert.Empty(replies);
            // We could fake it though! Setup a listener.
            var someIdentity = EnvironmentManager.Instance.RegisterReceiver(FakeCurrentWorkspaceRequest, new MessageIdFilter(messageId));
            // Now there is one listening for the message. 
            var fakeReplies = EnvironmentManager.Instance.SendMessage(new Message(messageId));
            Assert.NotEmpty(fakeReplies);
            Assert.Equal("Playback",fakeReplies.First());
            var rememberMe = EnvironmentManager.Instance.RegisterReceiver(FakeCurrentWorkspaceRequest, new MessageIdFilter(messageId));
            EnvironmentManager.Instance.UnRegisterReceiver(someIdentity);
        }

        public string FakeCurrentWorkspaceRequest(Message message, FQID destination, FQID sender) => "Playback";
        #endregion Messaging using the EnvironmentManager

        #region Messaging using the MessageCommunicationManager
        /* In the above a test the messaging provider is the EnvironmentManager.
         In the below test the provider is the an instance of the class MessageCommunication
         */

        /// <summary>
        /// The first 3 lines shows how to create an instants of the MessageCommunication class.
        /// This type of messaging is named "Mip messaging".
        /// Also, observer the specification of the server id. 
        ///
        /// The rest is similar but with slight differences. And one important thing to
        /// remember, the "TransmitMessage" method will not return a result.
        /// 
        /// And even more important, this messaging goes across applications.
        ///
        /// So, for example it is possible to message from a SmartClient to a plug-in
        /// loaded in the event server service.
        ///
        /// Remember that registrations are held in the event server service, that is, when
        /// doing "communicator.RegisterCommunicationFilter(...)" the information is kept in
        /// the event server. In a sense the event server service is the post office.
        ///
        /// Please contrast the below with the test using the "EnvironmentManager".
        ///
        /// To make the below test succeed, it is necessary to make time for the
        /// registration to complete, and for the "TransmitMessage" to complete. All in all
        /// this message provider is more powerful, goes across application borders, but is slower
        /// and uses more resources. Use it only when going across application borders is needed. 
        /// </summary>
        [Fact]
        public void SendMipMessageTest()
        {
            // Make the initial mysterious incantation
            var serverId = EnvironmentManager.Instance.MasterSite.ServerId;
            MessageCommunicationManager.Start(serverId);
            var communicator = MessageCommunicationManager.Get(serverId);

            var messageId = "com.acme.MessageIdSample";
            var obj = communicator.RegisterCommunicationFilter(ReceiveMyMipMessagePlease, new CommunicationIdFilter(messageId));
            AllowSomeTimeForTheRegistrationToComplete();
            // using the communicator send a message to your self
            communicator.TransmitMessage(new Message(messageId), null, null, null);

            var received = Wait();

            communicator.UnRegisterCommunicationFilter(obj);
            Assert.True(received, "Make sure the event server is running");
        }
        #endregion Messaging using the MessageCommunicationManager
        public void Dispose()
        {

        }
        #region Helper methods
        private bool Wait()
        {
            var count = 0;
            var received = false;
            while (!received && count < 100)
            {
                received = mipMessagingReceiverMarker == "Received Mip Message";
                count++;
                Thread.Sleep(500);
            }

            return received;
        }
        private object ReceiveMyMipMessagePlease(Message message, FQID destination, FQID sender)
        {
            mipMessagingReceiverMarker = "Received Mip Message";
            return "MessageReceived";
        }
        private void AllowSomeTimeForTheRegistrationToComplete() => Thread.Sleep(3.Seconds());
        /// <summary>
        /// Message handler
        /// </summary>
        /// <param name="message"></param>
        /// <param name="destination"></param>
        /// <param name="sender"></param>
        /// <returns></returns>
        private object ReceiveMyMessagePlease(Message message, FQID destination, FQID sender) => "MessageReceived";
        #endregion helper methods
    }
}
