﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Extensions.Messaging;
using Extensions.SystemObjects;
using VideoOS.Platform;
using VideoOS.Platform.Messaging;
using Xunit;

namespace Test.Messaging
{
    /// <summary>
    /// Test showing usage of the "SimpleMessaging" class. This class makes usage 
    /// of the two messaging providers nearly identical.
    /// 
    /// It comes with some costs.
    /// 
    /// It hides that only EnvironmentManager messaging returns a result.
    /// It also hides some of the functionality provided by the messaging providers. The providers have a
    /// more sophisticated message id filtering system as an example.
    ///
    /// I favor the simplicity of the "SimpleMessaging" class, over the more powerful native interface.
    ///
    /// Example: In a plug-in there is an "Init" and a "Close" method. These are lifetime management 
    /// methods, called on application startup and shutdown.
    ///
    /// Please contrast the "Init" and "Close" against the "InitNative" and "InitClose" and observe the
    /// larger footprint of the latter, then pick your favorite approach. 
    /// </summary>
    public partial class  MessagingTest
    {
        private const string MessageId = "com.acme.MessageIdSample";

        /// <summary>
        /// The below test uses the wrapper SimpleMessaging.
        /// The parameter to the constructor determines the type of messaging provider being used.
        /// Here it is "MessagingScope.Internal" which maps to EnvironmentManager messaging.
        /// Compare this test with "SendMethodTest" in NativeMessagingTest.
        ///
        /// The close method, here "messenger.Close()" will un-register everything registered on
        /// class.  
        /// </summary>
        [Fact]
        [Trait("Category", "Wrapped Messaging")]
        public void SendMethodWrapperVersionTest()
        {
            var messenger = new SimpleMessaging(MessagingScope.Internal);
            // start listening
            messenger.Register(ReceiveMyMessagePlease, MessageId);
            // waits for all registered receivers to reply! 
            var returnValue = messenger.Send(MessageId);
            Assert.True((string)returnValue.First() == "MessageReceived");
            // Don't introduce memory leaks...
            messenger.Close();
        }

        /// <summary>
        /// Recall that EnvironmentManager.Instance.SendMessage call, returns a reply from
        /// all listeners.
        /// Using "SimpleWrapper" it is not possible to register the same message id with twice.
        /// So, in this test to have multiple listeners there is a bit of cheating.
        ///  
        /// </summary>
        [Fact]
        [Trait("Category", "Wrapped Messaging")]
        public void MultipleListenersWrapperVersionTest()
        {
            var messenger = new SimpleMessaging(MessagingScope.Internal);
            // Register a message handler and start listening
            messenger.Register(ReceiveMyMessagePlease, MessageId);
            var obj = EnvironmentManager.Instance.RegisterReceiver(ReceiveMyMessagePlease, new MessageIdFilter(MessageId));

            // Send a message and wait for all listeners to reply. 
            var returnValues = messenger.Send(MessageId);
            var replyCount = 2;
            Assert.Equal(replyCount, returnValues.Count());
            // Un-register. Don't introduce memory leaks...
           messenger.Close(); 
        }
        /// <summary>
        /// The message handler signature has three parameters. Two of them are almost never used.
        /// The "SimpleMessaging" class allows for message handlers to have a shorter signature.
        /// Exemplified in this test.
        /// </summary>
        [Fact]
        [Trait("Category", "Wrapped Messaging")]
        public void SendMethodShortSignatureTest()
        {
            var messenger = new SimpleMessaging(MessagingScope.Internal);
            // start listening
            messenger.Register((Func<Message, string>)(p => "MessageReceived"), MessageId);
            // waits for all registered receivers to reply! 
            var returnValue = messenger.Send(MessageId);
            Assert.True((string)returnValue.First() == "MessageReceived");
            // Don't introduce memory leaks...
            messenger.Close();
        }
       
        /// <summary>
        /// This uses the external scope. Observe that setting up the messaging is hidden
        /// within the "SimpleMessaging" class.
        /// </summary>
        [Fact]
        [Trait("Category", "Wrapped Messaging")]
        public void SendMipMessageUsingWrapperTest()
        {
            var messenger = new SimpleMessaging(MessagingScope.External);
            // start listening
            messenger.Register(ReceiveMyMipMessagePlease, MessageId);
            AllowSomeTimeForTheRegistrationToComplete();
            messenger.Send(MessageId);

            var received = Wait();
            // Don't introduce memory leaks...
            messenger.Close();
            Assert.True(received, "Make sure the event server is running");
        }

        private int res = 0;

        [Fact]
        [Trait("Category", "Wrapped Messaging")]
        public async Task SimpleMessagingMathTest()
        {
            var messenger = new SimpleMessaging(MessagingScope.External);
            messenger.Register((Func<Tuple<int, int>, int>)Add, "Add");
            await Task.Delay(5.Seconds());
            messenger.Send("Add", new Tuple<int, int>(2, 3));
            await Task.Delay(5.Seconds());
            Assert.Equal(5, res);
            messenger.Register((Func<Tuple<int, int>, int>)((t) => res = t.Item1 + t.Item2), "Add1");
            await Task.Delay(5.Seconds());
            messenger.Send("Add1", new Tuple<int, int>(26, 3));
            await Task.Delay(5.Seconds());
            Assert.Equal(29, res);
            messenger.Close();
        }

        #region Init/Close method examples for either a native or wrapped approach.
        // wrapping messaging code 
        private int Add(Tuple<int, int> arg) => res = arg.Item1 + arg.Item2;
        private int Subtract(Tuple<int, int> arg) => res = arg.Item1 - arg.Item2;
        
        private ISimpleMessaging myMessenger;
        private void Init()
        {
            myMessenger = new SimpleMessaging(MessagingScope.External);
            myMessenger.Register((Func<Tuple<int, int>, int>)Add, "Add");
            myMessenger.Register((Func<Tuple<int, int>, int>)Subtract, "Subtract");
        }

        private void Close() => myMessenger.Close();

        // Using messaging in a native manner
        // Observe that the below variables is needed in order to be able to un-register.
        private object handlerAdd;
        private object handlerSubtract;
        private void InitNative()
        {
            handlerAdd = EnvironmentManager.Instance.RegisterReceiver(HandlerAdd, new MessageIdFilter("Add"));
            handlerSubtract = EnvironmentManager.Instance.RegisterReceiver(HandlerSubtract, new MessageIdFilter("Subtract"));
        }

        private void CloseNative()
        {
            EnvironmentManager.Instance.UnRegisterReceiver(handlerAdd);
            EnvironmentManager.Instance.UnRegisterReceiver(handlerSubtract);
        }

        private object HandlerAdd(Message message, FQID destination, FQID sender) =>
            ((Tuple<int, int>) message.Data).Item1 + ((Tuple<int, int>) message.Data).Item2;
        private object HandlerSubtract(Message message, FQID destination, FQID sender) =>
            ((Tuple<int, int>)message.Data).Item1 + ((Tuple<int, int>)message.Data).Item2;


        #endregion Init/Close method examples for either a native or wrapped approach.
    }
}
