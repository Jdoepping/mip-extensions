﻿namespace Extensions.Messaging
{
    /// <summary>
    /// <para>
    /// Scope is either "External" or "Internal"
    /// When messaging across application borders use "External". Example between the event server and the management client.
    /// When messaging within application borders use "Internal". Example two plug-ins loaded by a SmartClient.
    /// </para>
    /// </summary>
    public enum MessagingScope
    {
        /// <summary>
        /// Use external messaging for example when sending messages from a standalone application.
        /// Or when messaging "broadly", when you believe it cannot be handled by Internal messaging. 
        /// </summary>
        External,
        /// <summary>
        /// Use the Internal.Instance as the means of messaging. This limits the messaging scope 
        /// to within the current application e.g. within the stand alone application or within the smart client, management client etc. 
        /// </summary>
        Internal,
    }
}