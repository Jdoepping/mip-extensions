﻿using System;
using VideoOS.Platform;
using VideoOS.Platform.Messaging;

namespace Extensions.Messaging
{
    public interface ISimpleMessaging : IDisposable
    {
        MessageCommunication ExternalMessenger { get; set; }
        void Register(MessageReceiver handler, string messageFilter);
        void Register<T>(Action<T> handler, string messageFilter);
        void Register<T, TR>(Func<T, TR> func, string messageId);
        /// <summary>
        /// DeRegister a single messageId.
        /// </summary>
        /// <param name="messageId"></param>
        void DeRegister(string messageId);
        /// <summary>
        /// Use this method only when the object should stay alive after the call (Very unlikely that you will need this.) 
        /// </summary>
        void DeRegister();
        /// <summary>
        /// Using this method signals that the object is at the end of its lifetime.
        /// </summary>
        void Close();

    }
}