﻿using System;
using System.Collections.Generic;
using VideoOS.Platform;
using VideoOS.Platform.Messaging;

namespace Extensions.Messaging
{
    /// <summary>
    /// <para>
    /// A wrapper around the two messaging methods (Mip or Internal).
    /// </para><para>
    /// It unifies the usage of the two messaging methods.
    /// It simplifies the lifetime management of the messaging provider and cleans up the code. It does on the surface introduce
    /// limitations because it hides functionality provided by the messaging frameworks. This limitations are easily overcome though.
    /// </para>
    /// <para>It should simplify and reduce the amount of code you need to write</para>
    ///
    /// It handles either messaging done using MessageCommunicator or messaging done using Internal.Instance. Not 
    /// both at the same time.
    /// 
    /// It reduces the amount of code you need to write and handles all de-registering in a single call (DeRegister).
    /// 
    /// Usage: Add local variable of type SimpleMessaging: SimpleMessaging messagingWrapper;
    /// 
    /// Add to the Init method:
    ///             messagingWrapper = new SimpleMessaging(MessagingScope. )
    ///             add messagehandlers using the Register method
    /// 
    /// </summary>
    public class SimpleMessaging : ISimpleMessaging
    {
        private readonly MessagingScope type;
        private readonly Dictionary<string, object> receivers;
        private readonly ServerId serverId;
        private MessageCommunication communicator;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type">Specifies which type of messaging provider is used</param>
        public SimpleMessaging(MessagingScope type) : this(type, EnvironmentManager.Instance.MasterSite.ServerId)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type">Specifies which type of messaging provider is used</param>
        /// <param name="serverId">Specify the vms to connect to. Use this constructor when you are not connecting to Internal.Instance.MasterSite.ServerId</param>
        public SimpleMessaging(MessagingScope type, ServerId serverId)
        {
            this.type = type;
            this.serverId = serverId;
            receivers = new Dictionary<string, object>();
        }
        public MessageCommunication ExternalMessenger
        {
            get
            {
                if (communicator != null) return communicator;
                if (type==MessagingScope.Internal) throw new Exception("You cannot use this property with an instance initialized with MessagingScope.Internal");
                MessageCommunicationManager.Start(serverId);
                communicator = MessageCommunicationManager.Get(serverId);
                return communicator;
            }
            set => communicator = value;
        }

        public void Register<T>(Action<T> handler, string messageFilter) =>
            RegisterHelper((m, d, s) =>
            {
                handler((T)m.Data);
                return null;
            }, messageFilter);
        public void Register<T,TR>(Func<T,TR> func, string messageId)=> RegisterHelper((m, d, s) => func((T)m.Data), messageId);
         public void Register(MessageReceiver handler, string messageId) => RegisterHelper(handler, messageId);
       

        public ICollection<object> Send(string messageId) => SendHelper(new Message(messageId));
        /// <summary>
        /// Send method that unifies the way a message is send.
        /// Please be aware of the parameter hiding that takes place for both the TransmitMessage and SendMessage method, <see cref="SendHelper"/>
        /// Access to all the parameters for the two methods can be achieved using either
        /// the <see cref="ExternalMessenger"/> property of this class, or using  "Internal.Instance.SendMessage"
        /// directly, depending on what messaging provider you use.
        /// </summary>
        /// <param name="messageContent"></param>
        /// <param name="messageId"></param>
        /// <returns></returns>
        public ICollection<object> Send(string messageId, object messageContent) => SendHelper(new Message(messageId, messageContent));
       
        public void DeRegister()=> Close();
      
        public void Close()
        {
            var action = UnRegisterMethod();
            foreach (var receiver in receivers.Values) action(receiver);
        }
      
        public void DeRegister(string messageId)
        {
            if (!receivers.ContainsKey(messageId)) return;

            var action = UnRegisterMethod();
            action(receivers[messageId]);
            receivers.Remove(messageId);
        }
        public void Dispose() => Close();
        private Action<object> UnRegisterMethod()
        {
            return type == MessagingScope.External ?
                (Action<object>)ExternalMessenger.UnRegisterCommunicationFilter :
                EnvironmentManager.Instance.UnRegisterReceiver;
        }
        private ICollection<object> SendHelper(Message message)
        {
            if (type != MessagingScope.External) return EnvironmentManager.Instance.SendMessage(message);
            ExternalMessenger.TransmitMessage(message, null, null, null);
            return new List<object>();
        }
        private void RegisterHelper(MessageReceiver handler, string messageId)
        {
            var receiver = type == MessagingScope.External ?
                ExternalMessenger.RegisterCommunicationFilter(handler, new CommunicationIdFilter(messageId))
                : EnvironmentManager.Instance.RegisterReceiver(handler, new MessageIdFilter(messageId));
            receivers.Add(messageId, receiver);
        }
    }
}
