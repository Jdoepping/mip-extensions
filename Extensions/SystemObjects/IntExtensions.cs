﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extensions.SystemObjects
{
    public static class IntExtensions
    {
        public static int Seconds(this int s) => 1000 * s;
    }
}
