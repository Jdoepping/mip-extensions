﻿using System;
using VideoOS.Platform.Messaging;

namespace Extensions.SmartClientNotification
{
    /// <summary>
    /// A notification/message in the Smart Client.
    /// A notification is displayed over the workspace. To see an example, search the help for "Smart Client message area tester"
    /// select "Smart Client message area tester". There will be a snapshot of a Smart Client with notifications/messages.
    /// </summary>
    public static class SmartClientMessageDataExtensions
    {
        public static void RemoveMessage(this SmartClientMessageData message) => message.Message = string.Empty;
        public static void UpdateProgressbar(this SmartClientMessageData message, double newValue)
        {
            if (newValue < 0 || newValue > 1) throw new ArgumentOutOfRangeException(nameof(newValue));
            message.TaskProgress = newValue;
        }
        public static void RemoveButton(this SmartClientMessageData message) => message.ButtonText = string.Empty;

    }
}
